//empty value focus

function isemptyfocus(name)
{
if($('#'+name).val().trim().length<=0)
 {
           $('#'+name).focus().css({
            'border-color':' red',
            '-webkit-border-radius': '4px',
             '-moz-border-radius': '4px',
                  'border-radius': '4px',
          '-webkit-box-shadow': '0px 0px 4px red',
             '-moz-box-shadow': '0px 0px 4px red',
                  'box-shadow': '0px 0px 4px red'
          });
     return true;        
  }else{
    return false;
  }
}
function onfocus(name)
{
     
 $('#'+name).focus().css({
  'border-color':' red',
  '-webkit-border-radius': '4px',
   '-moz-border-radius': '4px',
        'border-radius': '4px',
'-webkit-box-shadow': '0px 0px 4px red',
   '-moz-box-shadow': '0px 0px 4px red',
        'box-shadow': '0px 0px 4px red'
});
             

}

//empty value

function isempty(name,message)
{
if($('#'+name).val().trim().length<=0)
 {
          swal({title:"<strong style='color:#d9534f;'>Please Enter "+message+"!</strong>",
            html:true
          });
     return true;        
  }else{

    return false;

  }

}


//character only

function ischar(name,message="Valid Value")
{


if (!($('#'+name).val()).match(/^[a-zA-Z]+$/))
 {
          swal({title:"<strong style='color:#d9534f;'>Please Enter "+message+"!</strong>",
            html:true
          });
     return true;        
  }else{

    return false;

  }


}


//no space allowed in string it allowed other all characters/number/specialchar etc.

function nospace(name,message="Valid Value")
{

var regexp = /^\S*$/;
if (!($('#'+name).val()).match(regexp))
 {
          swal({title:"<strong style='color:#d9534f;'>Please Enter "+message+"!</strong>",
            html:true
          });
     return true;        
  }else{

    return false;

  }


}


// enter characters words only no numbers allowed

function isword_char(name,message="Valid Value")
{


if (!($('#'+name).val()).match(/^([a-zA-Z]+\s)*[a-zA-Z]+$/))
 {
          swal({title:"<strong style='color:#d9534f;'>Please Enter "+message+"!</strong>",
            html:true
          });
     return true;        
  }else{

    return false;

  }


} 





// enter words only characters & number no allowed spacial character eg @,!,* etc.

function isnospecialchar(name,message="Valid Value")
{


if (!($('#'+name).val()).match(/^[\w\s\,]*$/i))
 {
          swal({title:"<strong style='color:#d9534f;'>Please Enter "+message+"!</strong>",
            html:true
          });
     return true;        
  }else{

    return false;

  }


}




//Number only

function isnumber(name,message="Valid Value")
{


if (!($('#'+name).val()).match(/^[+-]?\d+(\.\d+)?$/))
 {
          swal({title:"<strong style='color:#d9534f;'>Please Enter "+message+"!</strong>",
            html:true
          });
     return true;        
  }else{

    return false;

  }


}



//Number only no - or + allowed

function isonlynumber(name,message="Valid Value")
{


if (!($('#'+name).val()).match(/^\d+(\.\d+)?$/))
 {
          swal({title:"<strong style='color:#d9534f;'>Please Enter "+message+"!</strong>",
            html:true
          });
     return true;        
  }else{

    return false;

  }


}



//email


function isemail(name,message="Valid Email")
{


if (!($('#'+name).val()).match(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/))
 {
          swal({title:"<strong style='color:#d9534f;'>Please Enter "+message+"!</strong>",
            html:true
          });


     return true;        
  }else{

    return false;

  }


}



//Mobile Number with country code and 10 digits 




function ismobile(name,message="Valid Value")
{


if (!($('#'+name).val()).match(/^([0|\+[0-9]{1,5})?([7-9][0-9]{9})$/))
 {
          swal({title:"<strong style='color:#d9534f;'>Please Enter "+message+"!</strong>",
            html:true
          });
     return true;        
  }else{

    return false;

  }


}



//Mobile Number only 10 digits 




function ismobile10(name,message="Valid Value")
{


if (!($('#'+name).val()).match(/^([7-9][0-9]{9})$/))
 {
          swal({title:"<strong style='color:#d9534f;'>Please Enter "+message+"!</strong>",
            html:true
          });
     return true;        
  }else{

    return false;

  }


}


// it allows to press only numeric key

function only_number_press(name)
{

  $('#'+name).keypress(function (e) {
     //if the letter is not digit then display error and don't type anything
     if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
        //display error message
        //$("#errmsg").html("Digits Only").show().fadeOut("slow");
        //alert("Only Numbers Are Allowed");
               return false;
    }
    });


}


//match password to confirm password

function confirmpassword(password,confirmpassword)
{


if (($('#'+password).val())!=($('#'+confirmpassword).val()))
 {
          swal({title:"<strong style='color:#d9534f;'>Password Does Not Mathch Please Re-Enter Password!</strong>",
            html:true
          });
     return true;        
  }else{

    return false;

  }


}




//**************************************************************************************************************************************
//****************************** Other Usable Functions ******************************************************************************
//**************************************************************************************************************************************


// 1.remove spaces from strings

function removespaces(value)
{

  var str = value.replace(/\s/g,''); 
  return str;

}


// auto remove bs-alert wich affected on alert calass

function auto_remove_alert()
{

$(document).ready(function () {
 
window.setTimeout(function() {
    $(".alert").fadeTo(1000, 0).slideUp(1000, function(){
        $(this).remove(); 
    });
}, 6000);
 
});


}

//hello




// change border red color when onchange input
function remove_border_onchange()
{

      jQuery(document).ready(function($) {
       
       /*   
        $("input").keypress(function(){
          $(this).css("border-color", "");
      });
      */

        $("input").change(function(){
          $(this).css({
                    'border-color':'',
                    '-webkit-border-radius': '',
                     '-moz-border-radius': '',
                          'border-radius': '',
                  '-webkit-box-shadow': '',
                     '-moz-box-shadow': '',
                          'box-shadow': ''
                  });
      });

        });
}





/* ajex Loader Function */

/* 1st arguments:-

        var loader_image='<?php echo base_url('/assets/img/ajax-loader.gif'); ?>';

    2nd arguments:-
       if you want to put images then----- "var loader_text='<img src="<?php echo base_url('/assets/img/loding-text.gif'); ?>">';
       if you want to put text then------- var loader_text="Loading...";


*/

function load_ajex_loader(loader_image,loader_text)
{

function ajaxindicatorstart(text)
  {
    if(jQuery('body').find('#resultLoading').attr('id') != 'resultLoading'){
    jQuery('body').append('<div id="resultLoading" style="display:none"><div><img src="'+loader_image+'"><div>'+text+'</div></div><div class="bg"></div></div>');
    }

    jQuery('#resultLoading').css({
      'width':'100%',
      'height':'100%',
      'position':'fixed',
      'z-index':'10000000',
      'top':'0',
      'left':'0',
      'right':'0',
      'bottom':'0',
      'margin':'auto'
    });

    jQuery('#resultLoading .bg').css({
      'background':'#000000',
      'opacity':'0.7',
      'width':'100%',
      'height':'100%',
      'position':'absolute',
      'top':'0'
    });

    jQuery('#resultLoading>div:first').css({
      'width': '250px',
      'height':'75px',
      'text-align': 'center',
      'position': 'fixed',
      'top':'0',
      'left':'0',
      'right':'0',
      'bottom':'0',
      'margin':'auto',
      'font-size':'16px',
      'z-index':'10',
      'color':'#ffffff'

    });

      jQuery('#resultLoading .bg').height('100%');
        jQuery('#resultLoading').fadeIn(300);
      jQuery('body').css('cursor', 'wait');
  }
        
        
        function ajaxindicatorstop()
  {
      jQuery('#resultLoading .bg').height('100%');
        jQuery('#resultLoading').fadeOut(300);
      jQuery('body').css('cursor', 'default');
  }




jQuery(document).ajaxStart(function () {
      //show ajax indicator
    ajaxindicatorstart(loader_text);
  }).ajaxStop(function () {
    //hide ajax indicator
    ajaxindicatorstop();
  });
  
  
  jQuery.ajax({
   global: false,
   // ajax stuff
});


}



/* end ajex loader */

