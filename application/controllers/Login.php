<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->library('encrypt');
		$this->load->model('Loginmodal');
	}
	public function index()
	{
		/*if(!(empty($this->session->userdata('UserID') || $this->session->userdata('BranchID')))){ redirect('Dashboard');} */
		$this->form_validation->set_message('required', '<i style="color:red;  margin:-2px;">Please Enter Valid Username or Password !</i>');
		$this->form_validation->set_rules('username', 'Username', 'required');
		$this->form_validation->set_rules('password', 'Password', 'required');		
		if ($this->form_validation->run() == FALSE)
		{
			$this->load->view('login_view');
			
		}
		else
		{
			//$pass=$this->encrypt->encode($this->input->post('password')); 
			$pass=$this->input->post('password');
			$where = array('UserName' => $this->input->post('username'));
			$username=$this->Loginmodal->get_row('tbllogin',$where,"Password,Id");
			$dbpass=$this->encrypt->decode($username[0]->Password);
			if ($pass==$dbpass) {
					$whe = array('Id' => $username[0]->Id);
					$session_set = array(
						'Id' => $username[0]->Id,
						'UserName'=> $username[0]->UserName
					);
					$this->session->set_userdata( $session_set );
					redirect(base_url().'Dashboard');
			}else{

				$this->session->set_flashdata('login_error','<i style="color:red;  margin:-2px;">Please Enter Valid Username or Password !</i>');
				redirect(base_url().'login');
			}
		}
	}
	public function Logout ()
	{
		$array_items = array('Id' => '', 'UserName' => '');
		$this->session->unset_userdata($array_items);
		$this->session->sess_destroy();
		redirect(base_url().'login');
	}		
	public function signup()
	{
		$this->load->view('signup');
	}
}