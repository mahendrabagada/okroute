<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Signup extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->library('encrypt');
		$this->load->model('Signupmodel');
	}
	public function index()
	{
		$this->load->view('signup');
	}
	public function add_data()
	{
		$data = $this->input->post();
		unset($data['ConfirmPassword'],$data['signup']);
		$data['Password'] = $this->encrypt->encode($data['Password']);
		$this->form_validation->set_rules('FirstName', 'First Name', 'required|is_unique[tblregister.UserName]');
		$this->form_validation->set_rules('LastName', 'Last Name', 'required');
		$this->form_validation->set_rules('UserName', 'UserName', 'required');
		$this->form_validation->set_rules('Password', 'Password', 'required');
		$this->form_validation->set_rules('ConfirmPassword', 'Confirm Password', 'required');
		$this->form_validation->set_rules('PhoneNumber', 'Phone Number', 'required|numeric');
		if ($this->form_validation->run() == TRUE)
		{
			$res = $this->Signupmodel->insert_record($data);
			if($res > 0)
			{
				$session_set = array(
								'Id' => $res,
								'UserName'=> $data['UserName']
							);
				$this->session->set_userdata( $session_set );
				redirect(base_url().'Dashboard');
			}
			else
			{
				$error = "Registration UnSuccessfull , Something Went Wrong";
				$this->session->set_flashdata('phperror',$error);
			}
		}
		else
		{
			$this->session->set_flashdata('phperror', 'Input Data is invalid please try again.<br>'.validation_errors());
			redirect(base_url('Signup'));
		}
		
	}
}