<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sender extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Sendermodel');
	}
	public function index()
	{
		$sender['data'] = $this->Sendermodel->getsender();
		$this->load->view('sender_view',$sender);
	}
	public function save_process()
	{
		$data = array('SenderName' => $this->input->post('SenderName'));
		// echo "<pre>";
		// print_r($data); exit;
		$this->form_validation->set_rules('SenderName', 'Sender Name', 'required');
		if ($this->form_validation->run() == TRUE)
		{
			$res = $this->Sendermodel->insert_record($data);
			if($res > 0)
			{
				$this->session->set_flashdata('phpsuccsess', 'Sender Added Successfully.');
				redirect(base_url('Sender'));
			}
			else
			{
				$this->session->set_flashdata('phperror', 'Sender Not Added Successfully.');
				redirect(base_url('Sender'));
			}
		}
		else
		{
			//$this->load->view('sender_view');
			$this->session->set_flashdata('phperror', 'Input Data is invalid please try again.<br>'.validation_errors());
			redirect(base_url('Sender'));
		}
	}
	public function update_process()
	{
		$data = array('SenderName' => $this->input->post('SenderName') );
		$id = $this->input->post('id');

		$this->form_validation->set_rules('SenderName', 'Sender Name', 'required');
		if ($this->form_validation->run() == TRUE)
		{
			$res = $this->Sendermodel->update_record($id,$data);
			if($res > 0)
			{
				$this->session->set_flashdata('phpsuccsess', 'Sender Updated Successfully.');
				redirect(base_url('Sender'));
			}
			else
			{
				$this->session->set_flashdata('phperror', 'Sender Not Updated.');
				redirect(base_url('Sender'));
			}
		}
		else
		{
			//$this->load->view('sender_view');
			redirect(base_url('Sender'));
		}
	}
	public function all()
	{
	
	}
}