<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Usermodal');
		$this->load->library('encrypt');
	}

	public function index()
	{
		
		$data['userlist']=$this->Usermodal->getuser();
		$this->load->view('user_view',$data);
	}



	public function AddUserView()
	{
		$Where = array('BranchStatus' => 0 );
		$data['barnchlist']=$this->Usermodal->get_row("TblBranch",$Where);
		$this->load->view('add_user',$data);

	}
	public function AddUser()
	{




		

		//$this->form_validation->set_message('UserPassword', 'You must select a business');
		//$this->form_validation->set_message('required', '<i style="color:red;  margin:-2px;">Please Enter Valid Username or Password !</i>');
		$this->form_validation->set_rules('UserName', 'User Name', 'required|regex_match[/^\S*$/]');
		$this->form_validation->set_rules('UserPassword', 'User Password', 'required|regex_match[/^\S*$/]');
		$this->form_validation->set_rules('confirmpassword', 'Password Confirmation', 'required|regex_match[/^\S*$/]|matches[UserPassword]');
		$this->form_validation->set_rules('UserEmail', 'Email Address', 'required|valid_emails');
		$this->form_validation->set_rules('UserMobile', 'Phone Number', 'required|numeric|exact_length[10]');	

		if (($this->form_validation->run() == TRUE) && ($this->input->post('UserBranchID')!=0))
		{
			$UserName=$this->input->post('UserName');
			$pass=$this->input->post('UserPassword');
			$UserPassword=$this->encrypt->encode($pass);
			//echo $this->encryption->decrypt($UserPassword);
			$UserEmail=$this->input->post('UserEmail');
			$UserMobile=$this->input->post('UserMobile');
			$UserBranchID=$this->input->post('UserBranchID');

			$userdata = array('UserName' => $UserName,'UserPassword' => $UserPassword,'UserEmail' => $UserEmail,'UserMobile' => $UserMobile,'UserBranchID' => $UserBranchID);
			$insert_id=$this->Usermodal->insert_record("tbluser",$userdata);
 
			if (($insert_id>0) &&($insert_id!=null) ){

				$this->session->set_flashdata('phpsuccsess', 'User Added Successfully.');
				redirect(base_url('User'));

				
			}else{

			$this->session->set_flashdata('phperror', 'Input Data is invalid please try again.<br>'.validation_errors());
			$Where = array('BranchStatus' => 0 );
			$data['barnchlist']=$this->Usermodal->get_row("TblBranch",$Where);
			$this->load->view('add_user',$data);


			}


			
			
		}
		else
		{



			$this->session->set_flashdata('phperror', 'Input Data is invalid please try again.<br>'.validation_errors());
			$data['barnchlist']=$this->Usermodal->get_row("TblBranch","false");
			$this->load->view('add_user',$data);

		}

	}



	public function EditUser()
	{
		if(!$this->input->post("id")){
			redirect(base_url('User'));
								}
		$where = array('UserID' =>$this->input->post("id"));
		$user=$this->Usermodal->get_row("tbluser",$where); 

		foreach ($user as $users) {
                            	$username=$users->UserName;
                            	$password= $this->encrypt->decode($users->UserPassword);
                            	$email=$users->UserEmail;
                            	$mobile=$users->UserMobile;	
                            	$branchid=$users->UserBranchID;
                            	$userid=$users->UserID;
                            }

         $data['userdata']= array('username' =>$username,'password' =>$password,'email' =>$email,'mobile' =>$mobile,'userid' =>$userid,'branchid' =>$branchid );

        $Where = array('BranchStatus' => 0 );
		$data['barnchlist']=$this->Usermodal->get_row("TblBranch",$Where);
		$this->load->view('edit_user', $data);


	}



	public function UpadteUser()
	{
		

		$this->form_validation->set_rules('UserName', 'User Name', 'required|regex_match[/^\S*$/]');
		$this->form_validation->set_rules('UserPassword', 'User Password', 'required|regex_match[/^\S*$/]');
		$this->form_validation->set_rules('confirmpassword', 'Password Confirmation', 'required|regex_match[/^\S*$/]|matches[UserPassword]');
		$this->form_validation->set_rules('UserEmail', 'Email Address', 'required|valid_emails');
		$this->form_validation->set_rules('UserMobile', 'Phone Number', 'required|numeric|exact_length[10]');	

		if (($this->form_validation->run() == TRUE) && ($this->input->post('UserBranchID')!=0))
		{	
			$userid=$this->input->post('userid');
			$UserName=$this->input->post('UserName');
			$pass=$this->input->post('UserPassword');
			$UserPassword=$this->encrypt->encode($pass);
			//echo $this->encryption->decrypt($UserPassword);
			$UserEmail=$this->input->post('UserEmail');
			$UserMobile=$this->input->post('UserMobile');
			$UserBranchID=$this->input->post('UserBranchID');

			$userdata = array('UserName' => $UserName,'UserPassword' => $UserPassword,'UserEmail' => $UserEmail,'UserMobile' => $UserMobile,'UserBranchID' => $UserBranchID);
			$insert_id=$this->Usermodal->update_record("tbluser","UserID",$userid,$userdata);
			if (($insert_id>0) &&($insert_id!=null) ){

				$this->session->set_flashdata('phpsuccsess', 'User Updated Successfully.');
				redirect(base_url('User'));

				
			}else{

			$this->session->set_flashdata('phperror', 'Input Data is invalid please try again.<br>'.validation_errors());
			$Where = array('BranchStatus' => 0 );
			$data['barnchlist']=$this->Usermodal->get_row("TblBranch",$Where);
			$this->load->view('add_user',$data);


			}




	}
}


}

/* End of file controllername.php */
/* Location: ./application/controllers/controllername.php */