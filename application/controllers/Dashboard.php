<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	public function index()
	{
		$this->load->view('dashboard_view');
	}

}

/* End of file controllername.php */
/* Location: ./application/controllers/controllername.php */