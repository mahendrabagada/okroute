<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>OK ROUTE Telecom Operator SMS-VOIP-HLR</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    	<!-- Bootstrap core CSS -->
    <link href="<?php echo base_url('/assets/bootstrap/css/bootstrap.min.css'); ?>" rel="stylesheet">

	<!-- Font Awesome -->
	<link href="<?php echo base_url('/assets/css/font-awesome.min.css'); ?>" rel="stylesheet">	
	<!-- Pace -->
	<link href="<?php echo base_url('/assets/css/pace.css'); ?>" rel="stylesheet">
	
	<!-- Color box -->
	<link href="<?php echo base_url('/assets/css/colorbox/colorbox.css'); ?>" rel="stylesheet">
	
	<!-- Morris -->
	<link href="<?php echo base_url('/assets/css/morris.css'); ?>" rel="stylesheet">	
	
	<!-- Endless -->
	<link href="<?php echo base_url('/assets/css/endless.min.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('/assets/css/endless-skin.css'); ?>" rel="stylesheet">
	

	

	<!-- Le javascript ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
	
	<!-- Jquery -->
	<script src="<?php echo base_url('assets/js/jquery-1.10.2.min.js'); ?>"></script>

	<!-- Bootstrap -->
	<script src="<?php echo base_url('assets/bootstrap/js/bootstrap.js'); ?>"></script>
   
	<!-- Flot -->
	<script src="<?php echo base_url('assets/js/jquery.flot.min.js'); ?>"></script>
   
	<!-- Morris -->
	<script src="<?php //echo base_url('assets/js/rapheal.min.js'); ?>"></script>
	<script src="<?php// echo base_url('assets/js/morris.min.js'); ?>"></script>
	
	
	<!-- Colorbox -->
	<script src="<?php echo base_url('assets/js/jquery.colorbox.min.js'); ?>"></script>

	<!-- Sparkline -->
	<script src="<?php echo base_url('assets/js/jquery.sparkline.min.js'); ?>"></script>
	
	<!-- Pace -->
	<script src="<?php echo base_url('assets/js/uncompressed/pace.js'); ?>"></script>
	
	<!-- Popup Overlay -->
	<script src="<?php echo base_url('assets/js/jquery.popupoverlay.min.js'); ?>"></script>
	
	<!-- Slimscroll -->
	<script src="<?php echo base_url('assets/js/jquery.slimscroll.min.js'); ?>"></script>
	
	<!-- Modernizr -->
	<script src="<?php echo base_url('assets/js/modernizr.min.js'); ?>"></script>
	
	<!-- Cookie -->
	<script src="<?php echo base_url('assets/js/jquery.cookie.min.js'); ?>"></script>
	
	<!-- Endless -->
	<script src="<?php // echo base_url('assets/js/endless/endless_dashboard.js'); ?>"></script>
	<script src="<?php echo base_url('assets/js/endless/endless.js'); ?>"></script>

	<!-- Sweet Alert -->
	<!-- data table -->
	<link href="<?php echo base_url('assets/js/sweetalert/dist/sweetalert.css'); ?>" rel="stylesheet">
	<script src="<?php echo base_url('assets/js/sweetalert/dist/sweetalert.min.js'); ?>"></script>

	<!-- my validation -->
	<script src="<?php echo base_url('assets/js/validation.js'); ?>"></script>
	<script src="<?php echo base_url('assets/js/custom_validation_js.js'); ?>"></script>

	<!-- data table -->
	<link href="<?php echo base_url('assets/css/jquery.dataTables_themeroller.css'); ?>" rel="stylesheet">
	<script src="<?php echo base_url('assets/js/jquery.dataTables.min.js'); ?>"></script>


	<!-- fire bug script -->

	<!-- <script type='text/javascript' src='http://getfirebug.com/releases/lite/1.2/firebug-lite-compressed.js'></script> -->


		<!-- starting script function -->
	<script type="text/javascript">
		
	remove_border_onchange();
	auto_remove_alert();

	</script>
	<style type="text/css">
    .m:hover{cursor: pointer;}
	td{
	    /*background-color:white;*/
	}
	th{
	    font-weight: bold;
	    font-width:80px;
	    border-bottom-color:black;
	}
	.bt{

	    border-radius: 15px;

	}
	</style>
  </head>

  <body class="overflow-hidden">
	<!-- Overlay Div -->
	<div id="overlay" class="transparent"></div>
	
	<a href="#" id="theme-setting-icon"><i class="fa fa-cog fa-lg"></i></a>
	<div id="theme-setting">
		<div class="title">
			<strong class="no-margin">Skin Color</strong>
		</div>
		<div class="theme-box">
			<a class="theme-color" style="background:#323447" id="default"></a>
			<a class="theme-color" style="background:#efefef" id="skin-1"></a>
			<a class="theme-color" style="background:#a93922" id="skin-2"></a>
			<a class="theme-color" style="background:#3e6b96" id="skin-3"></a>
			<a class="theme-color" style="background:#635247" id="skin-4"></a>
			<a class="theme-color" style="background:#3a3a3a" id="skin-5"></a>
			<a class="theme-color" style="background:#495B6C" id="skin-6"></a>
		</div>
		<div class="title">
			<strong class="no-margin">Sidebar Menu</strong>
		</div>
		<div class="theme-box">
			<label class="label-checkbox">
				<input type="checkbox" checked id="fixedSidebar">
				<span class="custom-checkbox"></span>
				Fixed Sidebar
			</label>
		</div>
	</div><!-- /theme-setting -->

	<div id="wrapper" class="preload">
		<div id="top-nav" class="fixed skin-6">
			

			<a href="#" class="brand">
				<span>OK ROUTE </span>
				<span class="text-toggle"><!-- some string--></span>
				
			</a><!-- /brand -->
			<button type="button" class="navbar-toggle pull-left hide-menu" id="menuToggle">
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>					
			<button type="button" class="navbar-toggle pull-left" id="sidebarToggle">
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<ul class="nav-notification clearfix">
				<li class="profile dropdown">
					<a class="dropdown-toggle m" data-toggle="dropdown" >
						<strong>John Doe</strong>
						<span><i class="fa fa-chevron-down"></i></span>
					</a>
					<ul class="dropdown-menu">
						<li><a tabindex="-1" class="main-link logoutConfirm_open" href="#logoutConfirm"><i class="fa fa-lock fa-lg"></i> Log out</a></li>
					</ul>
				</li>
			</ul>
		</div><!-- /top-nav-->
		
		<aside class="fixed skin-6">
			<div class="sidebar-inner scrollable-sidebar">
				<div class="size-toggle">
					<a class="btn btn-sm" id="sizeToggle">
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</a>
					<a class="btn btn-sm pull-right logoutConfirm_open"  href="#logoutConfirm">
						<i class="fa fa-power-off"></i>
					</a>
				</div><!-- /size-toggle -->	
				<div class="user-block clearfix">
				<br>
					<img src="<?php echo base_url('assets/img/logo.jpg'); ?>" alt="User Avatar">
					<div class="detail">
						<br>
						<strong>Telecom Operator</strong>
						<br><br><br>
					</div>

				</div><!-- /user-block -->

				<div class="main-menu">
					<ul>
						<li <?php if(strcmp($_ci_view,"dashboard_view")==0){  echo ' class="active"';}?>>
							<a href="<?php echo base_url('Dashboard');?>">
								<span class="menu-icon">
									<i class="fa fa-desktop fa-lg"></i> 
								</span>
								<span class="text">
									Dashboard
								</span>
								<span class="menu-hover"></span>
							</a>
						</li>
						<li class="openable">
							<a href="#">
								<span class="menu-icon">
									<i class="fa fa-user-secret fa-lg"></i> 
								</span>
								<span class="text">
									Admin
								</span>
								<span class="menu-hover"></span>
							</a>
							<ul class="submenu">
								<li><a <?php if(strcmp($_ci_view,"user_view")==0 || strcmp($_ci_view, "edit_user")==0 || strcmp($_ci_view, "add_user")==0){  echo ' class="act"';}?>href="<?php echo base_url('User'); ?>"><span class="submenu-label">User</span></a></li>
								<li><a <?php if(strcmp($_ci_view,"branch_view")==0 ){  echo ' class="active"';}?>href="<?php echo base_url('Branch'); ?>"><span class="submenu-label">Branch</span></a></li>
								
							</ul>
						</li>
						
						<li <?php if(strcmp($_ci_view,"sender_view")==0 || strcmp($_ci_view, "add_sender")==0 || strcmp($_ci_view, "edit_sender")==0){  echo ' class="active"';}?>>
							<a href="<?php echo base_url('sender'); ?>">
								<span class="menu-icon">
									<i class="fa fa-users fa-lg"></i> 
								</span>
								<span class="text">
									Senders
								</span>
								<span class="badge badge-success bounceIn animation-delay5">9</span>
								<span class="menu-hover active"></span>
							</a>
						</li>
						<li <?php if(strcmp($_ci_view,"inventory_view")==0 || strcmp($_ci_view, "add_inventory")==0 || strcmp($_ci_view, "update_inventory")==0){  echo ' class="active"';}?>>
							<a href="<?php echo base_url('inventory'); ?>">
								<span class="menu-icon">
									<i class="fa fa-tag fa-lg"></i> 
								</span>
								<span class="text">
									Inventory
								</span>
								<span class="badge badge-success bounceIn animation-delay5">9</span>
								<span class="menu-hover"></span>
							</a>
						</li>
						
						<li <?php if(strcmp($_ci_view,"edit_chalan")==0 || strcmp($_ci_view, "chalan_view")==0 || strcmp($_ci_view, "add_chalan")==0){  echo ' class="active"';}?>>
							<a href="<?php echo base_url('chalan'); ?>">
								<span class="menu-icon">
									<i class="fa fa-book fa-lg"></i> 
								</span>
								<span class="text">
									Chalan
								</span>
								<span class="badge badge-success bounceIn animation-delay5">9</span>
								<span class="menu-hover"></span>
							</a>
						</li>
						<li <?php if(strcmp($_ci_view,"certificate_view")==0 || strcmp($_ci_view, "add_certificate")==0 || strcmp($_ci_view, "edit_certificate")==0 || strcmp($_ci_view, "single_certificate_view")==0){  echo ' class="active"';}?>>
							<a href="<?php echo base_url('certificate'); ?>">
								<span class="menu-icon">
									<i class="fa fa-plus fa-lg"></i> 
								</span>
								<span class="text">
									Certificate
								</span>
								<span class="badge badge-success bounceIn animation-delay5">9</span>
								<span class="menu-hover"></span>
							</a>
						</li> <!-- //if(!empty($ActiveMenu) && $ActiveMenu == "Invoice"){ echo ' class="active"'; } -->
						<li <?php if(strcmp($_ci_view,"invoice_view")==0 || strcmp($_ci_view, "add_invoice")==0 || strcmp($_ci_view, "edit_certificate")==0){  echo ' class="active"';}?>>
							<a href="<?php echo base_url('invoice'); ?>">
								<span class="menu-icon">
									<i class="fa fa-plus fa-lg"></i> 
								</span>
								<span class="text">
									Invoice
								</span>
								<span class="badge badge-success bounceIn animation-delay5">9</span>
								<span class="menu-hover"></span>
							</a>
						</li>
					</ul>
					
					
				</div><!-- /main-menu -->
			</div><!-- /sidebar-inner -->
		</aside>

		<!-- Logout confirmation -->
	<div class="custom-popup width-100" id="logoutConfirm">
		<div class="padding-md">
			<h4 class="m-top-none"> Do you want to logout?</h4>
		</div>

		<div class="text-center">
			<a class="btn btn-success m-right-sm" href="<?php echo base_url('login/logout'); ?>">Logout</a>
			<a class="btn btn-danger logoutConfirm_close">Cancel</a>
		</div>
	</div>

	<!-- Delete recors -->

	<form id="deleteform" method="POST" action="<?php echo base_url('User/delete_record'); ?>">
					
					<input type="hidden" id="delete_input_data" name="delete_input_data">

	</form>

	<script type="text/javascript">
	
		function delete_record(data)
		{

			$('#delete_input_data').val(data);
			$("#deleteform").submit();


		}

		function edit_record(id)
		{

			$('#edit_input_id').val(id);
			$("#editform").submit();

		}

	</script>




		



					
								
						
						
					
				