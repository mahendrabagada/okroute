<!DOCTYPE html>
<html lang="en">
<head>
<title>OK ROUTE Telecom Operator SMS-VOIP-HLR</title>
 <!-- Bootstrap core CSS -->
  <link href="<?php echo base_url('/assets/bootstrap/css/bootstrap.min.css'); ?>" rel="stylesheet">
  <link href="<?php echo base_url('/assets/css/bootstrap-responsive.min.css'); ?>" rel="stylesheet">
  <link href="<?php echo base_url('/assets/css/login.css'); ?>" rel="stylesheet">
  <!-- Font Awesome -->
  <link href="<?php echo base_url('/assets/css/font-awesome.min.css'); ?>" rel="stylesheet"> 
  <!-- Jquery -->
  <script src="<?php echo base_url('assets/js/jquery-1.10.2.min.js'); ?>"></script>
  <!-- Bootstrap -->
  <script src="<?php echo base_url('assets/bootstrap/js/bootstrap.js'); ?>"></script>
  <script src="<?php echo base_url('assets/js/backstretch.min.js'); ?>"></script>
  <!-- sweet alert -->
   <script src="<?php echo base_url('assets/js/sweetalert/dist/sweetalert.min.js'); ?>"></script>
   <link href="<?php echo base_url('assets/js/sweetalert/dist/sweetalert.css'); ?>" rel="stylesheet" type="text/css" />
 <!-- validation -->
  <script src="<?php echo base_url('assets/js/validation.js'); ?>"></script>
  <meta charset="utf-8">

<script type="text/javascript">
jQuery(document).ready(function($) {
  $.backstretch([
      "<?php echo base_url('assets/img/sms1.jpg'); ?>", 
       "<?php echo base_url('assets/img/sms2.jpg'); ?>", 
        "<?php echo base_url('assets/img/sms3.jpg'); ?>",
        "<?php echo base_url('assets/img/sms4.jpg'); ?>"
    ], {duration: 3000, fade: 750});
    
});
$(document).ready(function () {
$('#do_signup').click(function(){
  if(isemptyfocus('username') || isemptyfocus('password') ){
    return false;
  }else{
    return true;
  }
});
});
</script>
  </head>
  <body>
    <div class="navbar navbar-fixed-top">
      <div class="navbar-inner">
        <div class="container">
          <a class="brand" href="#"><img height="50px" width="80px" src="<?php echo base_url('assets/img/logo.jpg'); ?>" alt="okroute logo"><div style="width:10px;"></div>OK ROUTE Telecom Operator SMS-VOIP-HLR</a>
        </div>
      </div>
    </div>
    <div class="container">
      
        <div id="signup-wraper">
            <form class="form login-form" method="post" action=""><?php //echo base_url('login'); ?>
                <legend><span class="blue">Registration</span></legend>
                <div class="body">
                <div class="input-group">
                    <span class="input-group-addon" id="username-addon"><i class="fa fa-user" aria-hidden="true"></i></span>
                    <input type="text" id="FirstName" class="form-control" placeholder="FirstName" aria-describedby="username-addon" name="FirstName">
                  </div><br>
                  <div class="input-group">
                    <span class="input-group-addon" id="username-addon"><i class="fa fa-user" aria-hidden="true"></i></span>
                    <input type="text" id="LastName" class="form-control" placeholder="LastName" aria-describedby="username-addon" name="LastName">
                  </div><br>
                  <div class="input-group">
                    <span class="input-group-addon" id="username-addon"><i class="fa fa-user" aria-hidden="true"></i></span>
                    <input type="text" id="UserName" class="form-control" placeholder="UserName" aria-describedby="username-addon" name="UserName">
                  </div><br>
                <div class="input-group">
                    <span class="input-group-addon" id="password-addon"><i class="fa fa-lock" aria-hidden="true"></i></span>
                    <input id="password" type="password" class="form-control" placeholder="Password" aria-describedby="password-addon" name="password">
                  </div><br>
                  <div class="input-group">
                    <span class="input-group-addon" id="password-addon"><i class="fa fa-lock" aria-hidden="true"></i></span>
                    <input id="ConfirmPassword" type="ConfirmPassword" class="form-control" placeholder="ConfirmPassword" aria-describedby="ConfirmPassword-addon" name="ConfirmPassword">
                  </div><br>
                  <div class="input-group">
                    <span class="input-group-addon" id="username-addon"><i class="fa fa-user" aria-hidden="true"></i></span>
                    <input type="text" id="PhoneNumber" class="form-control" placeholder="PhoneNumber" aria-describedby="PhoneNumber-addon" name="PhoneNumber">
                  </div><br>
                   <div class="input-group">
                    <span class="input-group-addon" id="username-addon"><i class="fa fa-user" aria-hidden="true"></i></span>
                    <input type="text" id="Email" class="form-control" placeholder="Email" aria-describedby="Email-addon" name="Email">
                  </div><br>
                  <div class="input-group">
                    <span class="input-group-addon" id="username-addon"><i class="fa fa-user" aria-hidden="true"></i></span>
                    <input type="text" id="AddressLine1" class="form-control" placeholder="AddressLine1" aria-describedby="AddressLine1-addon" name="AddressLine1">
                  </div><br>
                   <div class="input-group">
                    <span class="input-group-addon" id="username-addon"><i class="fa fa-user" aria-hidden="true"></i></span>
                    <input type="text" id="AddressLine2" class="form-control" placeholder="AddressLine2" aria-describedby="AddressLine2-addon" name="AddressLine2">
                  </div><br>
                   <div class="input-group">
                    <span class="input-group-addon" id="username-addon"><i class="fa fa-user" aria-hidden="true"></i></span>
                    <input type="text" id="AddressLine3" class="form-control" placeholder="AddressLine3" aria-describedby="AddressLine3-addon" name="AddressLine3">
                  </div><br>
                  <div class="input-group">
                    <span class="input-group-addon" id="username-addon"><i class="fa fa-user" aria-hidden="true"></i></span>
                    <input type="text" id="City" class="form-control" placeholder="City" aria-describedby="City-addon" name="City">
                  </div><br>
                  <div class="input-group">
                    <span class="input-group-addon" id="username-addon"><i class="fa fa-user" aria-hidden="true"></i></span>
                    <input type="text" id="State" class="form-control" placeholder="State" aria-describedby="State-addon" name="State">
                  </div><br>
                  <div class="input-group">
                    <span class="input-group-addon" id="username-addon"><i class="fa fa-user" aria-hidden="true"></i></span>
                    <input type="text" id="ZIPCode" class="form-control" placeholder="ZIPCode" aria-describedby="ZIPCode-addon" name="ZIPCode">
                  </div>
                </div><br>
                <div class="footer">      
                    <button type="submit" id="do_signup" class="btn btn-success ">Login</button>
                    <a href="<?php echo base_url('login');?>" id="cancel" class="btn btn-success pull-right">Cancel</a>
                    <br><br>
                </div>
            </form>
        </div>
    </div>
  </body>
</html>
