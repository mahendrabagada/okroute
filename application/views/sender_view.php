<?php $this->load->view('include/header'); ?>
<style>
	.list-group-item {
    position: relative;
    display: block;
    padding: 10px 15px;
    margin-bottom: -1px;
    background-color: #415160;
    border: 1px solid #ddd;
}
</style>

<script>
$(document).ready(function () {
	$('.add-item').click( function(){
		var count = $('.form').length;
		if(count > 0){ $('.form').remove(); }
    var newRow =  $('<form class="form" method="post" action="<?php echo base_url('Sender/save_process');?>">'+
	'<label>Sender Name</label> '+
	'<div class="input-group"><span class="input-group-addon"><i class="fa fa-user" aria-hidden="true"></i></span>'+
	'<input type="text" class="form-control" id="SenderName" name="SenderName"></div><br>'+
	'<div class="footer"><button type="submit" id="Save" name="Save" class="btn btn-success" style="width:118px;">Save</button>'+
	'<a href="<?php echo base_url('"Sender"'); ?>" name="clear" class="btn btn-danger pull-right " style="width:100px;">Cancel</a><br><br></div> '+
	'</form>');
   	$('.itemtoadd').append(newRow);
   	$('.add-item').hide();
	        
	});
	$('.itemtoedit').on('click','.edit', function(){
		var id = $(this).data('id');
		var name = $(this).find('h3').text();
		var isVisible = $('.add-item').is(':visible');// if($('.add-item').hide() == true ){ $('.add-item').show() }
		if(isVisible != true){ $('.add-item').show(); }
		var count = $('.form').length;
		if(count > 0){$('.form').remove(); }
				var newRow =  $('<form class="form" method="post" action="<?php echo base_url('Sender/update_process');?>">'+
				'<label>Sender Name</label> '+
				'<div class="input-group"><span class="input-group-addon"><i class="fa fa-user" aria-hidden="true"></i></span>'+
				'<input type="text" class="form-control" id="SenderName" name="SenderName" value="'+name+'"></div><br>'+
				'<div class="footer"><button type="submit" name="Update" class="btn btn-success Update" style="width:118px;">Update</button>'+
				'<a href="<?php echo base_url('"Sender"'); ?>" name="clear" class="btn btn-danger pull-right " style="width:100px;">Cancel</a><br><br></div> <input type="hidden" name="id" value="'+id+'">'+
				'</form>');
				$('.itemtoadd').append(newRow);
		
	       // if(count == 1)
	       // 	{ return false;}
	});
	$('.itemtoadd').on('click','#Save,.Update', function(){
		if(isemptyfocus("SenderName"))
		{ return false; }
	});
});
</script>
<div id="main-container">
			<div id="breadcrumb">
				<ul class="breadcrumb" style="padding:8.3px;">
					 <li><i class="fa fa-home"></i><a href="<?php echo base_url('Dashboard');?>"> Home</a></li>
					 <li class="active">Senders</li>	 
				</ul>
			</div><!-- /breadcrumb-->

			<div class="main-header clearfix grey-container" style="padding:20px;" >
				<div class="page-title">
					<h3 class="no-margin">Sender View</h3><i class="fa fa-refresh"></i>
				</div><!-- /page-title --><button class="btn btn-primary add-item" style="margin-left:10px;">Add</button>
			</div><!-- /main-header -->
			<div class="padding-md">
				<!-- php message start-->
				<div class="row">
		            <div class="col-md-2"></div>
		               <div class="col-md-3">
		               <?php if( $phpsuccsess = $this->session->flashdata('phpsuccsess')):  ?>
		                   <div class="alert alert-success"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><?php echo $phpsuccsess;?></div>
		               <?php endif;?> 
		               <?php if( $phperror = $this->session->flashdata('phperror')):  ?>
		                   <div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><?php echo $phperror;?></div>
		               <?php endif;?> </div>
		        </div>

	               <!-- PHP message end-->
				<!-- Page Content Start-->
				<div class="row">
					<div class="col-md-3 itemtoedit">
						<?php foreach ($data as$value) {
						?>
							<a href="#" class="list-group-item edit" data-id="<?php echo $value['SenderId'];?>">
							<div class="list-group-item-text clearfix text-center"><!--  style="background-color: #5d7380;" -->
								<h3><?php echo $value['SenderName'];?></h3>
							</div>
						</a>
						<?php } ?>
					</div><!-- /.col -->
					<div class="col-md-6 itemtoadd"><span class="text-danger"><?php echo form_error('SenderName');?></span>
						<!-- <form class="form" method="post" action="#">
							<label>Sender Name</label>
			                    <div class="input-group">
			                    <span class="input-group-addon"><i class="fa fa-user" aria-hidden="true"></i></span>
			                    <input type="text" class="form-control" id="SenderName" name="SenderName">
		                  		</div><br>
		                  	<div class="footer">      
			                    <button type="submit" id="Save" name="Save" class="btn btn-success" style="width:118px;">Save</button>
			                    <a href="<?php //echo base_url('Sender'); ?>" name="clear" class="btn btn-danger pull-right " style="width:100px;">Cancel</a>
			                    <br><br>
		                	</div> 
						</form> -->
					</div><!-- /.col -->
					<div class="col-md-3"></div><!-- /.col -->
				</div>
				<!-- Page Content End-->
<?php $this->load->view('include/footer'); ?>			