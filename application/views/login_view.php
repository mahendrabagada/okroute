<!DOCTYPE html>
<html lang="en">
<head>
<title>OK ROUTE Telecom Operator SMS-VOIP-HLR</title>
 <!-- Bootstrap core CSS -->
  <link href="<?php echo base_url('/assets/bootstrap/css/bootstrap.min.css'); ?>" rel="stylesheet">
  <link href="<?php echo base_url('/assets/css/bootstrap-responsive.min.css'); ?>" rel="stylesheet">
  <link href="<?php echo base_url('/assets/css/login.css'); ?>" rel="stylesheet">
  <!-- Font Awesome -->
  <link href="<?php echo base_url('/assets/css/font-awesome.min.css'); ?>" rel="stylesheet"> 
  <!-- Jquery -->
  <script src="<?php echo base_url('assets/js/jquery-1.10.2.min.js'); ?>"></script>
  <!-- Bootstrap -->
  <script src="<?php echo base_url('assets/bootstrap/js/bootstrap.js'); ?>"></script>
  <script src="<?php echo base_url('assets/js/backstretch.min.js'); ?>"></script>
  <!-- sweet alert -->
   <script src="<?php echo base_url('assets/js/sweetalert/dist/sweetalert.min.js'); ?>"></script>
   <link href="<?php echo base_url('assets/js/sweetalert/dist/sweetalert.css'); ?>" rel="stylesheet" type="text/css" />
 <!-- validation -->
  <script src="<?php echo base_url('assets/js/validation.js'); ?>"></script>
  <meta charset="utf-8">

<script type="text/javascript">
jQuery(document).ready(function($) {
  $.backstretch([
      "<?php echo base_url('assets/img/sms1.jpg'); ?>", 
       "<?php echo base_url('assets/img/sms2.jpg'); ?>", 
        "<?php echo base_url('assets/img/sms3.jpg'); ?>",
        "<?php echo base_url('assets/img/sms4.jpg'); ?>"
    ], {duration: 3000, fade: 750});
    
});
$(document).ready(function () {
$('#do_login').click(function(){
  if(isemptyfocus('username') || isemptyfocus('password') ){
    return false;
  }else{
    return true;
  }
});
});
</script>
  </head>
  <body>
    <div class="navbar navbar-fixed-top">
      <div class="navbar-inner">
        <div class="container">
          <a class="brand" href="#"><img height="50px" width="80px" src="<?php echo base_url('assets/img/logo.jpg'); ?>" alt="okroute logo"><div style="width:10px;"></div>OK ROUTE Telecom Operator SMS-VOIP-HLR</a>
        </div>
      </div>
    </div>
    <div class="container">
      
        <div id="login-wraper">
            <form class="form login-form" method="post" action=""><?php //echo base_url('login'); ?>
                <legend><span class="blue">Login</span></legend>
                <div class="body">
                <div class="input-group">
                    <span class="input-group-addon" id="username-addon"><i class="fa fa-user" aria-hidden="true"></i></span>
                    <input type="text" id="username" class="form-control" placeholder="Username" aria-describedby="username-addon" name="username">
                  </div>
                <?php
                if ($this->session->flashdata('login_error'))
                  { 
                  echo $this->session->flashdata('login_error');
                 }
                 if(form_error('username')){echo form_error('username');}
                 else if(form_error('password')){echo form_error('password');}
                 else{echo "<br>";}
                  ?>
                <div class="input-group">
                    <span class="input-group-addon" id="password-addon"><i class="fa fa-lock" aria-hidden="true"></i></span>
                    <input id="password" type="password" class="form-control" placeholder="Password" aria-describedby="password-addon" name="password">
                  </div><br>
                  <a type="submit" href="<?php echo base_url('signup');?>" id="s" class="btn btn-primary pull-left">SignUp</a>
                  <a data-toggle="modal" href="#forget_pass" class="pull-right"> Forgot Password? </a>
                </div>
                <div class="footer">      
                    <button type="submit" id="do_login" class="btn btn-success">Login</button>
                    <br><br>
                </div>
            </form>
        </div>
    </div>

     <!-- View Information Modal -->
<div id="forget_pass" class="modal fade" role="dialog" tabindex='-1'>
    <div class="modal-dialog modal-md" >
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h3 class="modal-title">Forget Passsword</h3>
            </div>
            <div class="modal-body">
              Enter Your Email Here To Receive Your Password. <br>Your Password Will Be Sent On Your Email Id.
                <div class="row">
                  <div class="col-md-1"></div>
                  <div class="col-md-10">
                  <form class="form" method="post" action="<?php echo base_url('login/forgot_password'); ?>"><!-- Profile/forgot_password-->
                      <label>Email</label>
                          <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-envelope" aria-hidden="true"></i></span>
                            <input type="text" class="form-control" id="UserEmail" placeholder="Enter Email" name="UserEmail">
                          </div>
                          <br>
                          <div class="footer">      
                            <button type="submit" id="forget" name="forget" class="btn btn-success" style="width:118px;">Send</button>
                            <br><br>
                        </div>
                  </form>
                  </div>
                </div>
        </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>


  </body>
</html>
