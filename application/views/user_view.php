<?php include_once "header.php"; ?>
<style type="text/css">
    
td{
    /*background-color:white;*/
}
th{
    font-weight: bold;
    font-width:80px;
    border-bottom-color:black;
}
.bt{

	border-radius: 15px;

}

</style>

<div id="main-container">
            <div id="breadcrumb">
                <ul class="breadcrumb" style="padding:8.3px;">
                     <li><i class="fa fa-home"></i><a href="<?php echo base_url('dashboard'); ?>"> Home</a></li>
                     <li class="active">User</li>    
                </ul>
            </div><!-- /breadcrumb-->
            <div class="main-header clearfix grey-container shortcut-wrapper" style="padding:28px;" >
                <div class="page-title">
                    <h3 class="no-margin"><i class="fa fa-user fa-2x"></i> User</h3>
                </div><!-- /page-title -->
            </div><!-- /main-header -->

<div class="padding-md" >
<?php  
            $phpsuccsess = $this->session->flashdata('phpsuccsess');
            if(isset($phpsuccsess)) {     
                                
                                 echo '<center><div class="alert alert-success fade in" style="width:80%;">
                                  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                  <strong>Success! </strong> '.$phpsuccsess.'
                                </div></center>';
                              }

             $deleterror= $this->session->flashdata('deleteerror');
             if(isset($deleterror)) {     
                                
                                  echo '<center><div class="alert alert-danger fade in" style="width:80%;">
                                  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                  <strong>Error! </strong> '.$deleterror.'
                                </div></center>';
                              }
             ?>
<div class="panel panel-default table-responsive">
					<div class="panel-heading">
						<a class="btn btn-success bt" href="<?php echo site_url('User/AddUserView');?>"><i class="fa fa-user" aria-hidden="true" title="Add User"></i> Add User</a>

						<div class="inline-block m-top-xs pull-right">
						<span class="badge "><?php echo count($userlist); ?></span>
						<strong class="m-left-xs m-right-sm"> Users</strong>
						</div>
						
					</div>
					<div class="padding-md clearfix">
						<table class="table table-striped" id="dataTable" >
							<thead >
								<tr >
									<th>No</th>
									<th>User Name</th>
									<th>Email Address</th>
									<th>Mobile Number</th>
									<th>Branch Name</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
							<?php 
							$no=0;
							foreach ($userlist as $row) {
								$no++;
								
								$delete=$row->UserID.',tbluser,UserID,UserStatus,User';
								$deletedata=base64_encode($delete);
							echo '
								<tr>
									<td>'.$no.'</td>
									<td>'.$row->UserName.'</td>
									<td>'.$row->UserEmail.'</td>
									<td>'.$row->UserMobile.'</td>
									<td>'.$row->BranchName.'</td>
									<td>
									<button type="button" onclick="edit_record('.$row->UserID.');" title="Edit" class="btn btn-success btn-xs bt"> <i class="fa fa-pencil "></i></button>
									<button type="button" onclick="delete_record(\''.$deletedata.'\')" title="Delete" class="btn btn-danger btn-xs bt"> <i class="fa fa-trash "></i></button>
									</td>
								</tr>';
								}
								?>
								
							</tbody>
						</table>
					</div> <!-- /.padding-md -->

					<form id="editform" method="POST" action="<?php echo base_url('User/EditUser'); ?>">
					
					<input type="hidden" id="edit_input_id" name="id">

				 	</form>

				 	
				</div><!-- /panel -->




		<script>
		$(function	()	{
			$('#dataTable').dataTable( {
				"bJQueryUI": true,
				"sPaginationType": "full_numbers"
			});
			
			
		});
	</script>




<?php include_once 'footer.php'; ?>	