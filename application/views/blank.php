<?php $this->load->view('include/header'); ?>

<div id="main-container">
			<div id="breadcrumb">
				<ul class="breadcrumb" style="padding:8.3px;">
					 <li><i class="fa fa-home"></i><a href="<?php echo base_url('Dashboard');?>"> Home</a></li>
					 <li class="active">Senders</li>	 
				</ul>
			</div><!-- /breadcrumb-->
			<div class="padding-md">
				<!-- Page Content Start-->
				<div class="row">
					<div class="col-sm-6 col-md-3">
						<div class="panel-stat3 bg-danger">
							<h2 id="userCount" class="m-top-none">12</h2>
							<h5>Customers</h5>
							<i class="fa fa-arrow-circle-o-up fa-lg"></i><span class="m-left-xs">5% Higher than last week</span>
							<div class="stat-icon">
								<i class="fa fa-user fa-3x"></i>
							</div>
							<div class="refresh-button">
								<i class="fa fa-refresh"></i>
							</div>
							<div class="loading-overlay">
								<i class="loading-icon fa fa-refresh fa-spin fa-lg"></i>
							</div>
						</div>
					</div><!-- /.col -->
				</div>
				<!-- Page Content End-->
<?php $this->load->view('include/footer'); ?>				