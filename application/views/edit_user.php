<?php include_once "header.php"; ?>
<div id="main-container">
			<div id="breadcrumb">
				<ul class="breadcrumb" style="padding:8.3px;">
					 <li><i class="fa fa-home"></i><a href="<?php echo base_url('Dashboard'); ?>"> Home</a></li>
					 <li><i class="fa fa-user"></i><a href="<?php echo base_url('User'); ?>"> User</a></li>
					 <li class="active">Edit User</li>	 
				</ul>
			</div><!-- /breadcrumb-->
			<div class="main-header clearfix grey-container shortcut-wrapper" style="padding:28px;" >
				<div class="page-title">
					<h3 class="no-margin"> <i class="fa fa-user fa-2x"></i> Edit User</h3>
					
				</div><!-- /page-title -->
				
				<script type="text/javascript">

				 var ouname='<?php echo $userdata["username"]; ?>';
				 var opass='<?php echo $userdata["password"]; ?>';
				 var omobile='<?php echo $userdata["mobile"]; ?>';
				 var oemail='<?php echo $userdata["email"]; ?>';
				 var obranchid='<?php echo $userdata["branchid"]; ?>';

					
				function validateuser()
				{

					function selectbranch ()
					{
						
						if($('#UserBranchID').val()==0){

							swal({title:"<strong style='color:#d9534f;'>Please Select User Branch!</strong>",
					            html:true
					          });
							$('#UserBranchID').focus().css({
				                    'border-color':' red',
				                    '-webkit-border-radius': '4px',
				                     '-moz-border-radius': '4px',
				                          'border-radius': '4px',
				                  '-webkit-box-shadow': '0px 0px 4px red',
				                     '-moz-box-shadow': '0px 0px 4px red',
				                          'box-shadow': '0px 0px 4px red'
				                  });

							return true;
						}else{
							return false;

						}

					}

					if(
						isemptyfocus("UserName") ||
						nospace("UserName","User Name Whithout Space") ||
						isemptyfocus("UserPassword") ||
						nospace("UserPassword","Password Whithout Space") ||
						isemptyfocus("confirmpassword") ||
						nospace("UserPassword","Confirm Password Whithout Space") ||
						confirmpassword("UserPassword","confirmpassword")||
						isemptyfocus("UserMobile") ||
						only_number_press("UserMobile") ||
						ismobile10("UserMobile","Valid Mobile Number") ||
						isemptyfocus("UserEmail")||
						isemail("UserEmail") ||
						selectbranch ()

						)
					{
						
						
						return false;
					}
					else{

						if (
							ouname==$('#UserName').val() &&
							opass==$('#UserPassword').val() &&
							omobile==$('#UserMobile').val() &&
							oemail==$('#UserEmail').val() &&
							obranchid==$('#UserBranchID').val()
							) {
								swal({title:"<strong style='color:#d9534f;'>User Detail is Not Change!</strong>",
					            html:true
					          });

								return false;

							  }else{

							  	return true;

							  }

						
					}


				}


				</script>


			</div><!-- /main-header -->
			
			<div class="padding-md">
			<?php 
			$msg = $this->session->flashdata('phperror');

                              if(isset($msg)) {     
                                if (strcmp($msg,"User Added Successfully.")==0) {


                                  echo '<center><div class="alert alert-success fade in" style="width:80%;">
                                  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                  <strong>Success! </strong> '.$msg.'
                                </div></center>';
                              }else{


                                echo '<center><div class="alert alert-danger fade in" style="width:80%;">
                                  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                  <strong>Error! </strong> '.$msg.'
                                </div></center>'; 



                              }
                            } 

                     
                 
			 ?>
			<div class="jumbotron" style="background-color:white;">
				<div class="row">
				
					<div class="col-md-6">
						<form class="form login-form" onsubmit="return validateuser()" method="post" action="<?php echo base_url('User/UpadteUser'); ?>">
		                <input type="hidden" name="userid" value="<?php echo $userdata['userid']; ?>">
		                <div class="body">
		                <label>User Name :</label>
		                <div class="input-group">

		                    <span class="input-group-addon" id="user-addon"><i class="fa fa-user" aria-hidden="true"></i></span>
		                    
		                    <input type="text" id="UserName" value="<?php echo $userdata['username']; ?>" class="form-control notempty" placeholder="User Name"  name="UserName">
		                  </div>
		                  <br>
		                  <label>Password :</label>
		                <div class="input-group">

		                    <span class="input-group-addon" id="password-addon"><i class="fa fa-lock" aria-hidden="true"></i></span>
		                    
		                    <input type="text" id="UserPassword" value="<?php echo $userdata['password']; ?>" class="form-control notempty" placeholder="User Password"  name="UserPassword">
		                  </div>
		                	
		                  <br>
		                  <label>Confirm Password :</label>
		                <div class="input-group">

		                    <span class="input-group-addon" id="confirmpassword-addon"><i class="fa fa-eye" aria-hidden="true"></i></span>
		                    
		                    <input type="password" id="confirmpassword" value="<?php echo $userdata['password']; ?>" class="form-control notempty" placeholder="Confirm Password"  name="confirmpassword">
		                  </div>
		                	
		                  <br>
		               
		                  <label>Phone Number :</label>
		                  <div class="input-group">
		                    <span class="input-group-addon" id="userphonno-addon"><i class="fa fa-mobile" aria-hidden="true"></i></span>
		                    
		                    <input type="text" id="UserMobile" value="<?php echo $userdata['mobile']; ?>" class="form-control notempty" placeholder="User Phone Number" aria-describedby="custphonno-addon" name="UserMobile">
		                  </div>
		                  <br>
		                  <label> Email Address :</label>
		                  <div class="input-group">
		                    <span class="input-group-addon" id="custemail-addon"><i class="fa fa-envelope" aria-hidden="true"></i></span>
		                    
		                    <input type="text" id="UserEmail" value="<?php echo $userdata['email']; ?>" class="form-control notempty" placeholder="User Email Address" aria-describedby="custemail-addon" name="UserEmail">
		                  </div> 
		                  <br>
		                
		                  <label>User Branch :</label>
		                   <div class="input-group">
							<span class="input-group-addon" ><i class="fa fa-map-marker" aria-hidden="true"></i></span>
									<select name="UserBranchID" id="UserBranchID" class="form-control custtype">
										<option value="0" >Select User Branch</option>

										<?php  
										foreach ($barnchlist as $key => $branch) {

											if ($branch->BranchID==$userdata['branchid']) {

												echo '<option value="'. $branch->BranchID .'" selected>'.$branch->BranchName.'</option>';
											}else{
											echo '<option value="'. $branch->BranchID .'">'.$branch->BranchName.'</option>';
												}
										}
										?>


									</select>
		                	</div>
		                </div><!-- body-->
		                <br>
		                <div class="footer">      
		                    <button type="submit" id="add_cust" name="add_cust" onclick="validateuser()" class="btn btn-success" style="width:100px;">Update</button>
		                    <a type="reset" href="<?php echo base_url('User'); ?>" name="cl1" class="btn btn-danger pull-right" style="width:100px;">Cancel</a>
		                    <br><br>
		                </div>
            
            			
					</div><!-- /.col -->

					</form>
				</div><!-- /.row -->
				</div>



<?php include_once 'footer.php'; ?>				