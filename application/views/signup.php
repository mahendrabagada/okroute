<!DOCTYPE html>
<html lang="en">
  
<!-- Mirrored from minetheme.com/Endless1.5.1/register.html by HTTrack Website Copier/3.x [XR&CO'2013], Wed, 08 Jun 2016 07:01:12 GMT -->
<head>
    <meta charset="utf-8">
    <title>Registration</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Bootstrap core CSS -->
    <link href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css'); ?>" rel="stylesheet">

	<!-- Font Awesome -->
	<link href="<?php echo base_url('assets/css/font-awesome.min.css'); ?>" rel="stylesheet">
	
	<!-- Endless -->
	<link href="<?php echo base_url('assets/css/endless.min.css'); ?>" rel="stylesheet">
	 <!-- Le javascript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="<?php echo base_url('assets/js/validation.js'); ?>"></script>

    <!-- Jquery -->
	<script src="<?php echo base_url('assets/js/jquery-1.10.2.min.js'); ?>"></script>
    
    <!--Bootstrap-->
    <script src="<?php echo base_url('assets/bootstrap/js/bootstrap.min.js'); ?>"></script>
   
	<!-- Modernizr -->
	<script src="<?php echo base_url('assets/js/modernizr.min.js'); ?>"></script>
	
	<!-- Pace -->
	<script src="<?php echo base_url('assets/js/pace.min.js'); ?>"></script>
	
	<!-- Popup Overlay -->
	<script src="<?php echo base_url('assets/js/jquery.popupoverlay.min.js'); ?>"></script>
	
	<!-- Slimscroll -->
	<script src="<?php echo base_url('assets/js/jquery.slimscroll.min.js'); ?>"></script>
	
	<!--Cookie-->
	<script src="<?php echo base_url('assets/js/jquery.cookie.min.js'); ?>"></script>

	<!--Endless-->
	<script src="<?php echo base_url('assets/js/endless/endless.js'); ?>"></script>
	<script>
	$(document).ready(function () {
		$("#PhoneNumber,#ZIPCode").keypress(function (e) {
		     if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
		               return false;
		    }
		   });
		$('#do_signup').click( function(){
			var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
			var email = $("#Email").val();
			if(isemptyfocus("FirstName") || isemptyfocus("LastName") || isemptyfocus("UserName") || isemptyfocus("Password") || isemptyfocus("ConfirmPassword") || isemptyfocus("PhoneNumber"))
			{ 
				return false; 
			}
			else if (!email== "" && !filter.test(email)) 
		    {
		    	onfocus("Email");
			    return false;
			}
			else if(isemptyfocus("City"))
			{
				return false;
			}
			else if($('#chkd').is(":checked") == false)
			{
				$('#chkd').focus();
				return false;
			}
		});
	});
	</script>
	
  </head>

  <body>
	<div class="login-wrapper">
		<div class="text-center">
			<h2 class="fadeInUp animation-delay10" style="font-weight:bold">
				<span class="text-success">OK Route</span> <span style="color:#ccc; text-shadow:0 1px #fff">Telecom</span>
			</h2>
	    </div>
		<div class="login-widget animation-delay1">	
			<div class="panel panel-default">
				<div class="panel-heading">
					<i class="fa fa-plus-circle fa-lg"></i> Sign up
				</div>
				<div class="panel-body">
					<form class="form-login" method="post" action="<?php //echo base_url('signup/add_data');?>">
						<div class="form-group">
							<label>FirstName</label>
							<input type="text" placeholder="FirstName" name="FirstName" id="FirstName" class="form-control input-sm bounceIn animation-delay2" >
						</div><!-- /form-group -->
						<div class="form-group">
							<label>LastName</label>
							<input type="text" placeholder="LastName" name="LastName" id="LastName" class="form-control input-sm bounceIn animation-delay2" >
						</div><!-- /form-group -->
						<div class="form-group">
							<label>UserName</label>
							<input type="text" placeholder="UserName" name="UserName" id="UserName" class="form-control input-sm bounceIn animation-delay2" >
						</div><!-- /form-group -->
						<div class="form-group">
							<label>Password</label>
							<input type="password" placeholder="Password" name="Password" id="Password" class="form-control input-sm bounceIn animation-delay2" >
						</div><!-- /form-group -->
						<div class="form-group">
							<label>Confirm Password</label>
							<input type="password" placeholder="Confirm Password" name="ConfirmPassword" id="ConfirmPassword" class="form-control input-sm bounceIn animation-delay3">
						</div><!-- /form-group -->
						<div class="form-group">
							<label>Phone Number</label>
							<input type="text" placeholder="Phone Number" name="PhoneNumber" id="PhoneNumber" class="form-control input-sm bounceIn animation-delay4">
						</div><!-- /form-group -->
						<div class="form-group">
							<label>Email</label>
							<input type="text" placeholder="Email address" id="Email" class="form-control input-sm bounceIn animation-delay5">
						</div><!-- /form-group -->
						<div class="form-group">
							<label>Address</label>
							<textarea placeholder="Address" name="Address" id="Address" class="form-control input-sm bounceIn animation-delay5" row="3"></textarea>
						</div><!-- /form-group -->
						<div class="form-group">
							<label>City</label>
							<input type="text" placeholder="City" name="City" id="City" class="form-control input-sm bounceIn animation-delay5">
						</div><!-- /form-group -->
						<div class="form-group">
							<label>State</label>
							<input type="text" placeholder="State" name="State" class="form-control input-sm bounceIn animation-delay5">
						</div><!-- /form-group -->
						<div class="form-group">
							<label>ZIPCode</label>
							<input type="text" placeholder="ZIPCode" name="ZIPCode" id="ZIPCode" class="form-control input-sm bounceIn animation-delay5">
						</div><!-- /form-group -->
						<div class="form-group">
							<label class="label-checkbox">
								 <input type="checkbox" id="chkd" />
								 <span class="custom-checkbox info bounceIn animation-delay6"></span>
								 I accept the agreement.
							</label>
						</div><!-- /form-group -->

						<div class="seperator"></div>
						<div class="form-group">
							<div class="controls">
								Already have an account? <a href="<?php echo base_url('login');?>" class="primary-font login-link">Sign In</a>
							</div>
						</div><!-- /form-group -->
						<hr/>
						<div class="form-group">
							<div class="controls text-right">
								<button name="signup" class="btn btn-success btn-sm" id="do_signup">Sign up</button>
							</div>
						</div><!-- /form-group -->
					</form>
				</div>
			</div><!-- /panel -->
		</div><!-- /login-widget -->
	</div><!-- /login-wrapper -->

  </body>
</html>
