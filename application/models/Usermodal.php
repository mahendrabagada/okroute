<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Usermodal extends CI_Model {

public function __construct()
	{
		parent::__construct();
		//Do your magic here
	}



	public function getuser()
	{

		 $this->db->select('*');
		 $this->db->from('tbluser');
		 $this->db->where('UserRole!=',1);
		 $this->db->where('UserStatus',0);
		 
		 $this->db->order_by("tbluser.UserID", "desc");
		 $this->db->join('tblbranch', 'tblbranch.BranchID = tbluser.UserBranchID');
   		 $query = $this->db->get();
		 return $query->result();

	}

	public function alluser()
	{
		$query = $this->db->select('*')
				->from('tbluser')
				->get();

		return $query->num_rows();
	}


}

/* End of file Usermodal.php */
/* Location: ./application/models/Usermodal.php */