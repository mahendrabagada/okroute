<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Loginmodal extends CI_Model {


public function __construct()
{
	parent::__construct();
	$this->load->database();
	//Do your magic here
}
public function get_row($table,$where,$select="*")
{
		$this->db->select($select);
		$this->db->from($table);
		if(!($where=="false" || $where==""))
		{
			$this->db->where($where);
		}
 		$query = $this->db->get();
 		$results=$query->result();
 		if (count($results)>0) {
 			return $results;
 		}else{
 			return false;
 		}
	}
}

/* End of file Loginmodal.php */
/* Location: ./application/models/Loginmodal.php */