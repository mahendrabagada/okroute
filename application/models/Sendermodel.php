<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Sendermodel extends CI_Model {


	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		//Do your magic here
	}
	public function getsender()
	{
		$this->db->select('*');
		$this->db->from('tblsender');
		$this->db->where('IsDeleted',0);
		$res = $this->db->get();
		return $res->result_array();
	}
	public function insert_record($data)
	{
		$this->db->insert('tblsender',$data);
		return $this->db->insert_id();
	}
	public function update_record($id,$data)
	{
		$this->db->where('SenderId',$id);
		$this->db->update('tblsender',$data);
		return true;
	}
	public function get_row($table,$where,$select="*")
	{
			$this->db->select($select);
			$this->db->from($table);
			if(!($where=="false" || $where==""))
			{
				$this->db->where($where);
			}
	 		$query = $this->db->get();
	 		$results=$query->result();
	 		if (count($results)>0) {
	 			return $results;
	 		}else{
	 			return false;
	 		}
		}
}

/* End of file Sendermodel.php */
/* Location: ./application/models/Sendermodel.php */