<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Signupmodel extends CI_Model {


	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		//Do your magic here
	}
	public function insert_record($data)
	{
		$this->db->insert('tblregister',$data);
		return $this->db->insert_id();
	}
}

/* End of file Signupmodel.php */
/* Location: ./application/models/Signupmodel.php */