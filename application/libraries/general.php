<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class general {

	function __construct() {
		$CI =& get_instance();
	}
	function get_total_no($table){
		global $CI;
		$query=$CI->db->Select('count(IsDeleted) as total')
					->where('IsDeleted',0)
					->get($table);
		$result= $query->result();
		return $result[0]->total;
	}
	function active_class($controller){
		global $CI;
		return $CI->router->fetch_class() == $controller?'class="active"':'';
	}
}
?>
